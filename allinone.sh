#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

Green_font_prefix="\033[32m" && Red_font_prefix="\033[31m" && Font_color_suffix="\033[0m"
Info="${Green_font_prefix}[信息]${Font_color_suffix}"
show_server_info() {
  echo "${Info}""查看服务器配置..."
  wget -N --no-check-certificate "https://gitlab.com/none_null/v/-/raw/master/one-key/show-config.sh" && chmod +x show-config.sh && ./show-config.sh

}
install_zsh(){
  echo "${Info}""安装zsh..." | tee /etc/motd
  apt install -y zsh && sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

}
install_bbr-plus() {
  echo "${Info}""安装bbr-plus..." | tee /etc/motd
  wget -N --no-check-certificate "https://gitlab.com/none_null/v/-/raw/master/one-key/bbr-plus.sh" && chmod +x bbr-plus.sh && ./bbr-plus.sh
}

install_trojan-go() {
  echo "${Info}""安装trojan-go..." | tee /etc/motd
  wget -N --no-check-certificate "https://gitlab.com/none_null/v/-/raw/master/one-key/trojan-go.sh" && chmod +x trojan-go.sh && bash trojan-go.sh
}

install_cloud-torrent() {
  echo "${Info}""安装cloud-torrent..." | tee /etc/motd
  wget -N --no-check-certificate "https://gitlab.com/none_null/v/-/raw/master/one-key/cloudt.sh" && chmod +x cloudt.sh && bash cloudt.sh
}

auto-dd() {
  echo "${Info}""重装系统DD..." | tee /etc/motd
  wget -N --no-check-certificate "https://gitlab.com/none_null/v/-/raw/master/dd/AutoDD.sh" && chmod +x AutoDD.sh && bash AutoDD.sh
}

nat() {
  echo "${Info}""端口转发..." | tee /etc/motd
  wget -N --no-check-certificate "https://gitlab.com/none_null/v/-/raw/master/one-key/socat.sh" && chmod +x socat.sh && bash socat.sh
}

tls() {
  echo "${Info}""安装证书..." | tee /etc/motd
  wget -N --no-check-certificate "https://gitlab.com/none_null/v/-/raw/master/one-key/tls.sh" && chmod +x tls.sh && bash tls.sh
}

welcome_info(){
  apt install -y boxes
  echo "^_^  Hi, have a nice day" | boxes -d diamonds -a hcvc | tee /etc/motd

}

echo && echo -e "VPS一键安装脚本 ${Red_font_prefix}${Font_color_suffix}



${Green_font_prefix}0.${Font_color_suffix} 查看服务器配置
${Green_font_prefix}1.${Font_color_suffix} 安装bbr-plus
${Green_font_prefix}2.${Font_color_suffix} 安装trojan-go
${Green_font_prefix}3.${Font_color_suffix} 安装cloud-torrent
${Green_font_prefix}4.${Font_color_suffix} 重装系统DD
${Green_font_prefix}5.${Font_color_suffix} 端口转发
${Green_font_prefix}6.${Font_color_suffix} 安装zsh
${Green_font_prefix}7.${Font_color_suffix} 安装证书


————————————" && echo

echo
rm *.sh
welcome_info
read -e -p " 请输入数字 [0-7]:" num

if [ -z "${num}" ];then
	num=2
fi

case "$num" in
0)
  show_server_info
  ;;

1)
  install_bbr-plus
  ;;

2)
  install_trojan-go
  ;;
3)
  install_cloud-torrent
  ;;
4)
  auto-dd
  ;;
5)
  nat
  ;;
6)
  install_zsh
  ;;
7)
  tls
  ;;
*)
  echo "请输入正确数字 [0-6]"
  ;;
esac
